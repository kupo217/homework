package com.example.homework

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }


   private fun init(){

       val generateButton = findViewById<Button>(R.id.generateButton)
       val randomNumberTV = findViewById<TextView>(R.id.randomNumberTV)
       generateButton.setOnClickListener {
           val myNumber = randomNumber()

           d("randomNumber", "My Random Number: $myNumber")

           if (myNumber%5 ==0 && myNumber/5 > 0){
               randomNumberTV.text = "Yes $myNumber"
           }else{
               randomNumberTV.text = "No $myNumber"
           }
       }
   }

   private fun randomNumber(): Int {
       return (-100..100).random()
   }


}